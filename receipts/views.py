from django.shortcuts import render, redirect
from .models import ExpenseCategory, Account, Receipt
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceipt, ExpenseCatCreate, AccountCreate

# Create your views here.


@login_required
def receipts_view(request):
    tab = Receipt.objects.filter(purchaser=request.user)
    print(tab)
    context = {"tab": tab}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceipt()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_view(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"tab": category}
    return render(request, "receipts/categories.html", context)


@login_required
def account_view(request):
    account = Account.objects.filter(owner=request.user)
    context = {"account": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def expensecatcreate(request):
    if request.method == "POST":
        form = ExpenseCatCreate(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCatCreate()

    context = {
        "form": form,
    }

    return render(request, "receipts/categories/create.html", context)


@login_required
def accountcreate(request):
    if request.method == "POST":
        form = AccountCreate(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_view")
    else:
        form = AccountCreate()

    context = {
        "form": form,
    }

    return render(request, "receipts/accounts/create.html", context)
