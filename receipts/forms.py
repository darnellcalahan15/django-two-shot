from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class ExpenseCatCreate(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountCreate(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
