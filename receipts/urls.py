from django.urls import path
from .views import (
    receipts_view,
    create_receipt,
    category_view,
    account_view,
    expensecatcreate,
    accountcreate,
)


urlpatterns = [
    path("accounts/create/", accountcreate, name="create_account"),
    path("categories/create/", expensecatcreate, name="create_category"),
    path("accounts/", account_view, name="account_view"),
    path("categories/", category_view, name="category_list"),
    path("receipts/", receipts_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipts_view, name="home"),
]
